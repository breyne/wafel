# Wafel: Another Finite Elements Library

A set of modular finite elements tools to treat simple problems of mechanics. 

### Modules
- `elements`: iso-parametric finite elements classes.  
- `materials`: constitutive model classes.
- `assembly`: build Structure instances by combining nodes, connectivity and lists of elements/materials. 
- `solver`: numpy linalg applied to Structure instances


### References
The initial module structure is influenced by Tim Fuller's[fem with python](https://github.com/tjfulle/fem-with-python) library.
