
''' ==================================================================
    ********** SPAM: Spanwise Material Model *************************
    ==================================================================

Idea: we handle non-locality by building a coarse approximation on top of a classical finite element discretization.

'''

import numpy as np
from wafel import assembly as AS
import pdb

''' ==================================================================
    ASSEMBLY *********************************************************
'''

class Structure(AS.Structure):
    """Spanwise structure 

    A classical structure plus some more tools.

    """
    def __init__(self, nod, connect, elt, mat):
        ''' SPAM Structure constructor
        The purpose is to append to a normal Structure instance an additionnal set of members (data+functions)

        - subsetIdx   : the gp indices of the subset
        - subsetSize  : their amount
        - subsetTable : the correponding gaussTable
        - PoUMatrix   : what maps from the subset to the rest

                f_gp   =          [P]          *   f_sb

              | f_g1 |   | p_11 p_12 ... p_1M|  | f_s1 |
              | f_g2 |   | p_12 p_22 ... p_2M|  | f_s2 |
              | f_g3 |   | p_13 p_32 ... p_3M|  | ...  |
              | ...  |   | ...  ...  ... ... |  | f_sM |
              | ...  | = | ...  ...  ... ... |  
              | ...  |   | ...  ...  ... ... | 
              | ...  |   | ...  ...  ... ... |
              | ...  |   | ...  ...  ... ... |
              | f_gN |   | p_N1 p_N2 ... p_NM|

        '''

        # Assign the structure part, see base class in assembly
        AS.Structure.__init__(self, nod, connect, elt, mat)

        # Specify the spam part, that is the list of macro-elements
        # and the update matrix

        lc = mat.lc
        spa = .7*lc
        # the condition is lc+hmx/2 < spa < lc
        # where hmax is the biggest element size
        # and spa the grid spacing.

        # ------------------------------------------------------------
        # Find the extreme spatial values in which the material grid will be built
        xmin=np.zeros(elt.dim)
        xmax=np.zeros(elt.dim)

        for d in range(elt.dim):
            xmin[d] = np.min(nod[:,d]) - .0*spa
            xmax[d] = np.max(nod[:,d]) + .49*spa

        # build the material grid with spacing LC
        spans = []
        Xsize = 1
        for d in range(elt.dim):
            spans += [np.arange(xmin[d], xmax[d], spa)]
            Xsize *= np.size(np.arange(xmin[d], xmax[d], spa))

        X=np.zeros([Xsize,elt.dim])
        if elt.dim == 1: X = spans[0][:,None]
        elif elt.dim == 2:
            X1,X2 = np.meshgrid(spans[0],spans[1])
            X[:,0] = X1.flatten()
            X[:,1] = X2.flatten()
        elif elt.dim == 3:
            X1,X2,X3 = meshgrid(spans[0],spans[1],spans[2])
            X[:,0] = X1.flatten()
            X[:,1] = X2.flatten()
            X[:,2] = X3.flatten()

        # ------------------------------------------------------------
        # Define partition of unity. 
        # What we need in practice is a matrix that maps from the subset SB to the rest of the gauss Points GP.
        # We use the ideal grid X that we just defined
        
        # To do so, we first compute all ifty-norms (we must define square-patches)
        gp2sb = np.zeros([self.nGaussPoints, len(X)])
        for i,gp in enumerate(self.gaussTable[:,1:]):
            for j,xi in enumerate(X):
                gp2sb[i,j] = np.max(abs(gp-xi))
                #pdb.set_trace()
        #print(gp2sb)

        # The Partition of Unity matrix contains weights of proximity 
        self.PoUMatrix = lc/2.-gp2sb
        #print(self.PoUMatrix)

        # since all particles from the grid are not useful, we can remove those who's support is outside the domain, that is the columns with only negative values, if any
        X = X[np.max(self.PoUMatrix,axis=0)>0,:]
        self.PoUMatrix = self.PoUMatrix[:,np.max(self.PoUMatrix,axis=0)>0]

        # Then we crop whatever is further than lc, that is less than 0
        self.PoUMatrix[self.PoUMatrix<0.] = 0.

        # Then we assign the same average value to all gaussPoints of an elt
        # More details: the superimposed approximation's order should match with the finite element approximation. Here we make it element-wise constant to match linear elements gradient shape functions.
        for el, con in enumerate(self.elems) :
            # find rows in gausstable
            gR = np.argwhere(self.gaussTable[:,0] == el)
            # Only if more than 1 gp per element
            self.PoUMatrix[gR,:] = np.mean(self.PoUMatrix[gR,:], axis=0)


        # Then normalize each line
        self.PoUMatrix /= np.sum(self.PoUMatrix, axis=1)[:,np.newaxis]

        # ------------------------------------------------------------
        # find the subset of gaussPoints as those closest to each point of the material grid
        grid2GP = np.zeros([len(X),self.nGaussPoints])
        for d in range(elt.dim):
            grid2GP += (X[:,d,None] - self.gaussTable[:,1+d])**2

        self.subsetIdx = np.sort(np.unique(grid2GP.argmin(axis=1)))# one index for each row
        self.subsetSize = len(self.subsetIdx)
        self.subsetTable = self.gaussTable[self.subsetIdx]  # one index for each row

        ## At last: make sure the phi_i(x_j)=delta_ij
        #pouFilter = [True]*self.subsetSize
        #for sb,gp in enumerate(self.subsetTable[:,0]):
        #    # Check if ideed phi_i(xi)=1
        #    pdb.set_trace()
        #    if self.PoUMatrix(gp,sb) !=1.:
        #        pouFilter[sb] = False
        #        # If not then find the other sb where it's not 0
        #        otherSb = self.PoUMatrix[gp,:]




    def subsetToGaussPoints(self, field):
        ''' Interpolate  field from subset to Gauss points

        Input
        ---------
        field: a np-array of length [subsetSize], dimension 1 or 2.
               In dim 2, the shape is [subsetSize, N]

        Ouptut
        ---------
        gpField : a  np-array of length (nGaussPoints), dimension 1 or 2.
                  In dim 2, the shape is (nGaussPoints, N)

        '''

        if len(field) != self.subsetSize:
            raise SizeError('Expecting ndarray of length (subsetSize)')
        return np.dot(self.PoUMatrix, field)

    def patch_integrate(self, field, sb_id, forgetPhi=None):
        ''' Integrate a function over the support of a given patch

            \int_{patch_i} f(x) phi_i dv
          = \sum_{gp_i   } f(gi) phi_i w_i 

        Beware as w_i is not the Gauss weight but the contribution of a given gausspoint its element's integration.

        Input
        ----------
        field: a gauss-wise list of values (2d-nd-array)
        sb_id: the specific patch we're looking at

        Output
        ----------
        INT: the integrated function
        
        '''

        INT = 0. # the array-size should adjust automatically
        
        # Get the concerned gauss points
        gp_id, = np.where(self.PoUMatrix[:,sb_id]>0.)

        # Loop over the concerned gausspoints
        for gp_id_current in gp_id :
            row = self.gaussTable[gp_id_current]

            #Identify element
            el_id = int(row[0])
            nod = self.nodes[self.elems[el_id]]
            elt = self.elemTypes[el_id]
            mat = self.material[el_id]

            # Get the index of that gp on it own element
            # To do so, we count the number of identical elem_id ABOVE IT in gaussTable
            gp_elid = len(np.where(self.gaussTable[:gp_id_current,0]==el_id ))

            # Get the contribution of that gauss point to the integral over that element.
            # That is, integrate a function that is one on it and 0 elsewhere.
            oneGP = np.zeros([len(elt.gaussPoints),1])
            oneGP[gp_elid] = 1.
            gp_contrib = np.sum(elt.integrate(oneGP, nod))

            # Get the PoU value at gp
            if forgetPhi is None: phi = self.PoUMatrix[gp_id_current,sb_id]
            else: phi = 1.

            # Increment integral
            INT += field[gp_id_current]*gp_contrib*phi
        return INT

    def plastic_mu(self, N):
        ''' I wish that matrix had a name

            mu_ij = \int_{\omega} phi_i N:C:B_x dv

            since \phi is non 0 only on \omega i, it should be pretty sparse

        '''
        # Ensure N is 2D
        if len(N.shape) is 1: N = N[:,None]

        # Inich'
        mu = np.zeros([self.K.shape[0], self.subsetSize ])

        # Smarter option (tested in 1D) ================================================
        # foreach sb we'll identify the nonzero phi and only intergate on the corresponding elements, then assemble
        # Definetly faster
        for i in range(self.subsetSize):

            #print("filling mu", i, '/', self.subsetSize)

            # list elements where the fct is not zero
            phi_i = self.PoUMatrix[:,i,None]  # ensure this is 2D
            elts = self.gaussTable[np.argwhere(phi_i != 0.)[:,0],0]
            elts  = np.unique(elts).astype(int)

            for (eeeeeeee, con) in enumerate(self.elems[elts]): 

                # Identify 
                e = elts[eeeeeeee] # the actual element index
                elt = self.elemTypes[e]
                mat = self.material[e]
                nod = self.nodes[con]

                # Build gauss repartition vector
                repG = np.where(self.gaussTable[:,0]==e)

                # Build repartition vector in global system
                rep = np.array([0]*len(con) * elt.dof )
                r = np.arange(self.dof)
                for (jj, n) in enumerate(con):
                    rep[jj*elt.dof+r] = n*elt.dof + r #

                # Increment mu
                mu[None,rep,i] += AS.equivForceElem(elt, nod, mat, phi_i[repG]*N[repG] )


        ## Brute force option (tested in 1D/2D ) ========================================
        ## Fill each column with equiPlasticforce made of (phi*N)
        #for i in range(self.subsetSize):
        #    print("filling mu", i, '/', self.subsetSize)
        #    phi_i = self.PoUMatrix[:,i,None] # ensure this is 2D
        #    mu[:,i] = self.equivForce(N*phi_i)

        return -mu

    def plastic_mutilde(self, N, span='local'):
        ''' I wish that matrix had a name

            mu_ij = \int_{\omega} phi_i N:C:B_x dv

            since \phi is non 0 only on \omega i, it should be pretty sparse

        '''
        # Ensure N is 2D
        if len(N.shape) is 1: N = N[:,None]

        # Inich'
        mu = np.zeros([self.subsetSize, self.K.shape[0]])

        # CASE: check the evolution law on average on patch support ============
        if span is 'patch':
            # Fill each row with equiPlasticforce made of (N*Indic(Omega_i))
            Indic = np.zeros([len(N),1])
            for i in range(self.subsetSize):
                # To gets the indicator of omega_i, we get phi and turn the nonzeros into ones. 
                Indic[:]=0.
                Indic[self.PoUMatrix[:,i] != 0.] = 1.
                mu[i,:] = self.equivForce(N*Indic)

        # CASE: check the evolution law at points only =========================
        elif span is 'local':
            for sb in range(self.subsetSize):

                #Identify element
                row = self.subsetTable[sb]
                el_id = int(row[0])
                con = self.elems[el_id]
                nod = self.nodes[con]
                elt = self.elemTypes[el_id]
                mat = self.material[el_id]

                # Get the index of that gp on it own element
                # To do so, we count the number of identical elem_id ABOVE IT in gaussTable
                gp_id_current = self.subsetIdx[sb]
                gp_elid = len(np.where(self.gaussTable[:gp_id_current,0]==el_id ))

                # Build repartition vector in global system
                rep = np.array([0]*len(con) * elt.dof )
                r = np.arange(self.dof)
                for (i, n) in enumerate(con):
                    rep[i*elt.dof+r] = n*elt.dof + r #

                # Compute & assemble
                xi = elt.gaussPoints[gp_elid]
                B_x = elt.shapeFunGradMatrix(xi, nod)
                D = mat.tangentOperator(elt.dim)
                mu[sb,rep] = np.dot(B_x.T,np.dot(D, N[gp_id_current]))

        else:
            raise WrongStringError("Expected arguments: 'local' or 'patch' ")

        return mu

    def plastic_psitilde(self, N, u):
        ''' N
        '''
        return np.dot(self.plastic_mutilde(N), u)

    def updateModulus(self, sig, deps, span='local'):
        if span is 'local': subset = self.subsetIdx 
        else: subset =list(range(self.nElems))
        for gp in subset:
            e = int(self.gaussTable[gp,0])
            self.material[e].updateTangentModulus(sig[gp], deps[gp])

    def plastic_lambda(self, N, span='local'):
        ''' I wish that matrix had a name

            lam_ij = - \int_i phi_j N:(h(sigma) I + C)::N dv

            since \phi is non 0 only on \omega i, it should be pretty sparse

        '''
        # Ensure N is 2D
        if len(N.shape) is 1: N = N[:,None]

        # Inich'
        lam = np.zeros([self.subsetSize, self.subsetSize])

        if span is 'patch':
            # first we recreate the function to integrate as a 2d gauss-wise np array
            fun = np.zeros(len(N))
            for g, el in enumerate(self.gaussTable[:,0]):
                mat = self.material[int(el)]
                TANG = mat.tangentOperator(self.dim) + mat.h
                fun[g] = np.dot(N[g], np.dot(TANG, N[g]))

            # To mimic that operation, we patch_integrate on omega_j the function multiplied by the indicator of omega_i
            # Since patch_integrate includes phi_j, we're fine
            Indic = np.zeros([len(N)])
            for i in range(self.subsetSize):
                # To get the indicator of omega_i, we get phi and turn the nonzeros into ones. 
                Indic[:]=0.
                Indic[self.PoUMatrix[:,i] != 0.] = 1.
                for j in range(self.subsetSize):
                    lam[i,j]=self.patch_integrate(fun*Indic, j)

        elif span is 'local':
            for i,gp in enumerate(self.subsetIdx):
                #Identify element
                row = self.subsetTable[i]
                el_id = int(row[0])
                con = self.elems[el_id]
                nod = self.nodes[con]
                elt = self.elemTypes[el_id]
                mat = self.material[el_id]
                phi = self.PoUMatrix[gp,i] # should be 1.

                # Compute and assemble
                TANG = mat.tangentOperator(self.dim) + mat.h
                lam[i,i] = phi*np.dot(N[gp], np.dot(TANG, N[gp]))

        else:
            raise WrongStringError("Expecting 'local or 'patch' as second argument")

        return -lam

    def updateAugmentedStiffness(self, N):
        K_aug1 = np.append(self.K, self.plastic_mu(N),axis=1)
        K_aug2 = np.append(self.plastic_mutilde(N), self.plastic_lambda(N),axis=1)
        self.Ka =  np.append(K_aug1,K_aug2,axis=0)

''' ==================================================================
    BOUNDARY CONDITIONS **********************************************
'''

class BoundaryConditions(AS.BoundaryConditions):
    ''' To the classical members of a BC instance we add the augmented part
    - Ksize: the size of K
    - SBsize: the number of potentially active subsets
    - totSize = Ksize + SBsize
    - activeSubsets: indices of active subsets in [0, SBsize]
    '''
    def __init__(self, uimp, fimp, ST, sb_idx=None):
        AS.BoundaryConditions.__init__(self,uimp, fimp, ST)

        self.Ksize = len(ST.K)
        self.SBsize = ST.subsetSize
        self.totSize = self.Ksize+self.SBsize

        self.renewActiveSubsets(sb_idx)
        self.updateRepartition()

    def updateRepartition(self):

        # Active subsets are appended to repF
        self.repFtot = np.array([False]*(self.totSize))
        self.repFtot[self.repF] = True
        self.repFtot[self.Ksize:] = self.activeSubsets

        self.repUtot = np.array([True]*(self.totSize))
        self.repUtot[self.repFtot] = False
        
        self.Uimptot = np.append(self.Uimp, np.zeros(np.sum(self.repUtot)-len(self.repU)))
        self.Fimptot = np.append(self.Fimp, np.zeros(np.sum(self.repFtot)-len(self.repF)))


    def addToActiveSubsets(self, idx):
        self.activeSubsets[idx] = True
        self.updateRepartition()

    def renewActiveSubsets(self, idx):
        self.activeSubsets=np.array([False]*self.SBsize)
        if idx is not None:
            self.activeSubsets[idx] = True
        self.updateRepartition()


''' ==================================================================
    SOLVERS **********************************************************
'''

def solveSystem(ST, BC, SM=None):
    """ 
    """
    # Ensure existence
    if not hasattr(ST, 'Ka'):
        raise missingUpdateError("Update augmented stiffness first")

    ## Zero-out the inactive-subsets rows of mutilde
    ## Inactive columns of mu should be zeroed by Dp_imp
    ## If not zero U will be to constrained
    zid = [False]*BC.totSize
    zid [BC.Ksize:] = BC.repUtot[BC.Ksize:]
    ST.Ka[zid,:] = 0.

    # Split Matrix
    Ka_uu = ST.Ka[ np.ix_(BC.repUtot, BC.repUtot) ]
    Ka_ff = ST.Ka[ np.ix_(BC.repFtot, BC.repFtot) ]
    Ka_uf = ST.Ka[ np.ix_(BC.repUtot, BC.repFtot) ]
    Ka_fu = ST.Ka[ np.ix_(BC.repFtot, BC.repUtot) ]

    if SM is None:
        Uunktot = np.linalg.solve( Ka_ff, BC.Fimptot - Ka_fu.dot(BC.Uimptot) )
        Funktot = Ka_uu.dot(BC.Uimptot)+ Ka_uf.dot(Uunktot)
    else: 
        Uunktot = np.linalg.solve( Ka_ff, BC.Fimptot + SM[BC.repFtot] - Ka_fu.dot(BC.Uimptot) )
        Funktot = Ka_uu.dot(BC.Uimptot)+ Ka_uf.dot(Uunktot) - SM[BC.repUtot] 

    # Split U and DP
    Uunk = Uunktot[0:len(BC.repF)]
    Funk = Funktot[0:len(BC.repU)]
    dp = np.zeros(BC.SBsize)
    dp[BC.activeSubsets] = Uunktot[len(BC.repF):]
    

    return Uunk, Funk, dp
