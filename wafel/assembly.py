
import numpy as np
from wafel import materials as MX
from wafel import elements as EL
import pdb



''' ==================================================================
    ********** STRUCTURE OBJECTS *************************************
'''

class Structure:
    """ Holds a problem materials and geometry."""

    def __init__(self, nod, connect, elt, mat):
        """ Default Structure constructor
        
        Attributes
        ----------
        nNodes:     number of nodes 
        dim:        number of dimensions
        nElems:     number of elements
        nodes:      coordinates: |X1 Y1 Z1|
                                 |X2 Y2 Z2|
                                 |.. .. ..|
        elems:      connectivity table
        elemTypes:  list of element instances (size nElems)
        dof:        dof per node
        material:   list of material instances (size nElems)
        K:          stiffness matrix
        gaussTable: [elt_index, coordinates] for each gausspoint
        nGaussPoints: the total number of Gauss points

        """
        # ============================================================
        # Nodes and connectivity
        # --- Based on meshio style of output
        self.nNodes = nod.shape[0]
        self.dim = nod.shape[1]
        self.nElems = connect.shape[0]
        self.nodes = nod
        self.elems = connect

        # ============================================================
        # Assign elements
        # --- First option: pass one element type for all of them
        if isinstance(elt, EL.FiniteElement):
            self.elemTypes = [elt]* self.nElems 
        # --- Second option: give a list with one instance per element
        elif type(elt) is list:
            if len(elt) == self.nElems:
                self.elemTypes = elt 
            else:
                raise SizeError("Wrong element list size, at least")
        else:
            raise TypeError("Expecting list of elements or single element type")

        # ============================================================
        # Evaluate problem dofs (same for all elements)
        self.dof = self.elemTypes[0].dof

        # ============================================================
        # Assign materials: same as for elementt type
        if isinstance(mat, MX.Material):
            self.material = [mat]* self.nElems 
        elif type(mat) is list:
            if len(elt) == self.nElems:
                self.material = mat
            else:
                raise SizeError("Wrong material list size, at least")
        else:
            raise TypeError("Expecting list of materials or single material instance")
        
        # ============================================================
        # Build stiffness matrix 

        self.updateStiffness()

        # ============================================================
        # Store Gauss Point table
        self.gaussTable = []
        tmp = [0]*(self.dof+1)
        for (el, connect) in enumerate(self.elems):
            coords = self.nodes[connect]
            for (g, xi) in enumerate(self.elemTypes[el].gaussPoints):
                tmp[0]=el
                tmp[1:]=np.dot(self.elemTypes[el].shapeFun(xi), coords)
                self.gaussTable += [tmp[:]]
        self.gaussTable = np.array(self.gaussTable)
        self.nGaussPoints = self.gaussTable.shape[0]

    def updateStiffness(self, arg=None):
        """  Update stiffness matrix or create if not existing.

        Input
        ----------
        arg: a list of quantities defined per gauss point

        Output
        ----------
        None
        """
        if hasattr(self, 'K'):
            self.K[:]=0
        else: 
            size = self.nNodes * self.dof
            self.K=np.zeros(shape=(size,size))
        for (el, connect) in enumerate(self.elems):
            coords = self.nodes[connect]
            # Build nodal/dof repartition vector
            rep = np.array([0]*len(connect) * self.dof )
            r = np.arange(self.dof)
            for (i, n) in enumerate(connect):
                rep[i*self.dof+r] = n*self.dof + r #
            if arg is None: curArg=None
            else: curArg = arg[np.where(self.gaussTable[:,0]==el)]
            self.K[np.ix_(rep,rep)] += stiffElem(self.elemTypes[el], coords, self.material[el], curArg)
        return

    def imposeAtNodes(self, dirIdx, fun, value=1.):
        """ returns a 3-columns table to help prep BCs
        Input
        ----------
        nod: 2d-np array of coordinates 
        dirIdx: integer index of direction (0-x, 1-y, etc.)
        fun: a boolean valued callable 
             e.g. fun = lambda x: x[:,1]<0
             will isolate all nodes on the left of the vertical axis 
    
        Output table
        ----------
        1st column: nodes satisfying
        """
        imp = []
        nodOk = fun(self.nodes)
        idxNodOk = np.where(nodOk)[0]
        for (n, idx) in enumerate(idxNodOk):
            imp += [[idx, dirIdx, value]]
        return imp

    def postProcess(self, U, optionalFields=None):
        """ Calls the appropriate post-processing function (from materials) and stacks up the results in a list.
        The returned list contains one set of solutions per elements.
        Each solution fields contains one set of value per Gauss point.
        
        Input
        ----------
        U: 1D_nparray, nodal displacements
        optionalFields: list of size nElems with whatever in to pass to post-processing, element-wise

        Output
        ----------
        A list of dictionnaries

        """
        # Initialize solutions
        solutions = [False]*self.nElems
        # Loop on elements
        for (e, con) in enumerate(self.elems):
            # Build nodal/dof repartition vector
            rep = np.array([0]*len(con) * self.dof )
            r = np.arange(self.dof)
            for (i, n) in enumerate(con):
                rep[i*self.dof+r] = n*self.dof + r #
            # Then postProcessElem is called from the material instance
            if optionalFields is None: opt = None
            else: opt = optionalFields[np.where(self.gaussTable[:,0]==e)]
            solutions[e] = (
                self.material[e].postProcessElem(
                    self.elemTypes[e], 
                    self.nodes[con], 
                    U[rep], 
                    opt
                )
            )
        return solutions

    def postProcessing(self, U, optionalFields=None):
        """ Calls the appropriate post-processing function (from materials) and stacks up the results in a list.
        The returned list contains one set of solutions per elements.
        Each solution fields contains one set of value per Gauss point.
        The behavior is probably undefined if different element types are considered
        
        Input
        ----------
        U: 1D_nparray, nodal displacements
        optionalFields: list of size nElems with whatever in to pass to post-processing, element-wise

        Output
        ----------
        A dictionnary containing solutions as np arrays

        """
        # Initialize solutions
        solutions = {}
        # Loop on elements
        for (e, con) in enumerate(self.elems):
            # Build nodal/dof repartition vector
            rep = np.array([0]*len(con) * self.dof )
            r = np.arange(self.dof)
            for (i, n) in enumerate(con):
                rep[i*self.dof+r] = n*self.dof + r #
            # Then postProcessElem is called from the material instance
            if optionalFields is None: opt = None
            else: opt = optionalFields[np.where(self.gaussTable[:,0]==e)]
            sols = (
                self.material[e].postProcessElem(
                    self.elemTypes[e], 
                    self.nodes[con], 
                    U[rep], 
                    opt
                )
            )
            # then each dictionnary value is assigned in the big dictionnary
            for key in sols: 
                if e==0:
                    solutions[key] = np.array(sols[key])
                else   : 
                    solutions[key] = np.append(solutions[key], sols[key], axis=0)
        return solutions
                
    def strainsAndStresses(self, U, EpsP=None):
        """ Specific post-processing, not quite appropriate for default cases in mechanics.

        Input
        ----------
        Same as postProcess
        Except EpsP is a np.array, one row per Gauss point

        Output
        ----------
        2D-np-arrays, one row per gauss pt, one column  per component

        """
        if self.dim == 1: comp = 1
        elif self.dim == 2: comp = 3
        elif self.dim == 3: comp = 6
        eps = np.zeros([self.nGaussPoints, comp])
        sig = np.zeros([self.nGaussPoints, comp])
        sols = self.postProcess(U, EpsP)
        for (e, con) in enumerate(self.elems):
            # Build GaussTable repartition vector
            repG = np.argwhere(self.gaussTable[:,0]==e)
            for i, g in enumerate(repG):
                sig[g] = sols[e]["stresses"][i]
                eps[g] = sols[e]["strains"][i]
        return eps, sig

    def integrate(self, fun, interpolate=False):
        """ Integrate the given function over the whole domain

        Input
        ----------
        fun: the local evaluations of the function to integrate
            2D-np.array, one row per point
            As many columns as components of the solution
            A scalar result is a 1D array
            Or one can pass np.array([[1]]) to output the weight vectors
        interpolate: 
            if False: then the evaluations are already at gauss points
            if True : then the evaluations are at nodes and must be interpolated

        Output
        ----------
        An integral result
        """
        # Build the gauss-wise values vectors
        weights = np.zeros([self.nGaussPoints, fun.shape[1]])
        i = 0
        for (e, con) in enumerate(self.elems):
            if interpolate:
                raise NotImplementedError("Sorry!")
            else: 
                fun_gp = fun
            for (n, xi) in enumerate(self.elemTypes[e].gaussPoints):
                weights[i] = (
                        self.elemTypes[e].gaussWeights[n] *
                        self.elemTypes[e].jacobian(xi, self.nodes[con]) * 
                        self.elemTypes[e].integCst
                        )
                i += 1
        return np.dot(fun_gp.T, weights)

    def gaussPoints2Nodes(self, gpField):
        """ Extrapolates fields from integration points to nodes 
        Concretely, each nodal value is a weighted average of the connected elements values.
        The weight is the length/surface/volume.
        """
        # Instantiating
        if len(gpField.shape) == 1: 
            #gpField = gpField[:,None]
            # Above is necessary for integration with list that components must be np.arrays themselves
            nodField = np.zeros(self.nNodes)
            nElemPerNode = np.zeros(self.nNodes)
        else: 
            nodField = np.zeros(shape = (self.nNodes, gpField.shape[1]))
            nElemPerNode = np.zeros([self.nNodes, gpField.shape[1]])

        for (el, con) in enumerate(self.elems):
            # Each elements brings its contribution to the connected node
            gp_id = np.argwhere(self.gaussTable[:,0]==el)
            one = np.ones(len(gp_id))[:,None]
            nodField[con] += self.elemTypes[el].integrate(gpField[gp_id],self.nodes[con]) / self.elemTypes[el].integrate(one,self.nodes[con]) 
            # ... And increment the nodal counter by one
            nElemPerNode[con] += 1.

        # At the end, we divide per the number of elements per node
        return nodField / nElemPerNode

    def scalarGradient(self, Field):
        """ Gimme a 
        Input
        ----------
        Field: a 1D np array of values per node

        Output
        ----------
        grad: a 2D np array of its gradients per Gauss point
        """
        # Initialize
        grad = np.zeros(shape=(self.nGaussPoints, self.dim))
        gp = 0
        # Loop over Gauss points
        for (el, con) in enumerate(self.elems):
            for (g, xi) in enumerate(self.elemTypes[el].gaussPoints):
                G = self.elemTypes[el].shapeFunGradMatrix(xi, self.nodes[con], True) # true is for scalars
                grad[gp] = np.linalg.norm( np.dot(G, Field[con]) )
                gp += 1
        return grad


    def equivForce(self, T):
        """ Compute the equivalent force field

        Psi = \int B_x^T [D] {T} dx
        by assembling elementary forces.
        If not all elements are subjected to instabilities then it will be awkward.

        Input
        ----------
        T: 2D-np.array, 1 row/gp, 1 col/component

        Output
        ----------
        Psi: the equivalent force at nodes
        
        """
        # Initialization
        Psi = np.zeros(len(self.K))

        # Loop on elements
        for (e, con) in enumerate(self.elems): 

            # Identify 
            elt = self.elemTypes[e]
            mat = self.material[e]
            nod = self.nodes[con]

            # Build gauss repartition vector
            repG = np.where(self.gaussTable[:,0]==e)

            # Build repartition vector in global system
            rep = np.array([0]*len(con) * self.elemTypes[e].dof )
            r = np.arange(self.dof)
            for (i, n) in enumerate(con):
                rep[i*elt.dof+r] = n*elt.dof + r #

            # Increment Psi
            Psi[None,rep] += equivForceElem(elt, nod, mat, T[repG])

        return Psi

''' ==================================================================
    ********** BOUNDARY CONDITIONS OBJECTS ***************************
'''

class BoundaryConditions:
    """ Define imposed forces and displacements on a domain."""
    def __init__(self, uimp, fimp, STR):
        """
        Input
        ----------
        uimp: prescribed displacement
              format:
              | node_index  dof_index  value |
              | ...         ...        ...   |
              | ...         ...        ...   |
              >(depends X 3)

        fimp: prescribed forces, same form
              There's a bit of additional work to fill 
              the final vector with zeros

        STR: a Structure instance

        Processed: 
        ----------
        Uimp, Fimp: values of uimp, fimp
        repU, repF: lists of indexes in the global system

        """
        dof = STR.dof
        size = STR.nNodes*STR.dof

        # Sort by node_index
        # source: https://gist.github.com/stevenvo/e3dad127598842459b68
        uimp = uimp[uimp[:,0].argsort()]
        if fimp is not None: fimp = fimp[fimp[:,0].argsort()]

        # Assign for U
        self.Uimp = uimp[:,2]
        self.repU = uimp[:,0].astype(int)*dof+uimp[:,1].astype(int)

        # Build repf as the complement of repu
        rf = np.arange(size)
        rf[self.repU] = -1
        self.repF = rf[rf!=-1]

        # assign Fimp with mostly zeros
        fi = np.zeros(size)
        if fimp is not None: fi[int((fimp[:,0])*dof+fimp[:,1])]=fimp[:,2]
        self.Fimp = fi[self.repF]

''' ==================================================================
    ********** ELEMENTARY ASSEMBLY ***********************************
'''
def stiffElem(elt, nod, mat, arg=None):
    """Compute stiffness matrix for a single element

    Input
    ----------
    elt: FiniteElement instance
    
    nod: nodal coordinates of the element vertices 
         format: 
         |X1 X2 X3 .. |
         |Y1 Y2 Y3 .. |
         |Z1 Z2 Z3 .. | >(nDim X nNodes)

    mat: material instance

    arg: an optional argument : gauss-wise quantity upon which the stiffness depends

    Output
    ----------
    Ke: the elementary stiffness matrix as:
        \int{el} B_x^T D B_x det(J) dxi

    """
    # Define "volumic power of internal forces"~ish

    # If we can build a function of xi only then we do it
    if arg is None: # then create a callable and loop only once 
        D = mat.tangentOperator(elt.dim)
        def pint(xi):
            # Below we evaluate B_x =/= B_xi. 
            # Super important. See function 
            B_x = elt.shapeFunGradMatrix(xi, nod)
            return np.dot(np.dot(B_x.T, D), B_x)

    # If the function depends upon other variables then a list must be created
    else: # then create a list and loop twice (once here and once at integration)
        pint = []
        for (n, xi) in enumerate(elt.gaussPoints[:]):
            D = mat.tangentOperator(elt.dim, arg[n])
            B_x = elt.shapeFunGradMatrix(xi, nod)
            pint += [np.dot(np.dot(B_x.T, D), B_x)]
    # Integrate
    return elt.integrate(pint,nod)


def equivForceElem(elt, nod, mat,  T):
    """ Build the equivalent plastic force for one element.
    
    We seek to compute the generic form 


    \int_e [B]^t [D]{T} dv

    which returns a force-like vector.
    - When {T} = {Eps} = [B]{U} in elasticity, it computes the internal forces
    - When {T} = {EpsP} 

    Input
    ----------
    - elt: the element type
    - nod: the nodes coords |X1 Y1 Z1| etc
    - T (2d array) : the bechterew tensor at gauss point
        |T11(g1) T22(g1) ...|
        |T11(g1) T22(g2) ...|
      ... or a list of gauss wise values

    Ouptut
    ----------
    > Psi_e = \int_{el} B_x^t [D] {T}

    """
    # Since T is given as a list we'll integrate by first building a point-wise list
    D = mat.tangentOperator(elt.dim)
    localforce = []
    for (n, xi) in enumerate(elt.gaussPoints[:]):
        B_x = elt.shapeFunGradMatrix(xi, nod)
        localforce +=  [np.dot(B_x.T, np.dot(D, T[n]))]
    return elt.integrate(localforce,nod)
