import sys
sys.path.append("../")

import pdb
import copy as CP
import numpy as np
from math import sqrt

ROOT2 = sqrt(2.)
ROOT3 = sqrt(3.)

######################################################################

class Material:
    """ A material base class (abstract)."""
    # What must be defined
    def tangentOperator(self, *args):
        """ What is used to build the linear system matrix"""
        raise NotImplementedError("Abstract method of base class FiniteElement")

    def postProcessElem(self, *args):
        """ Returns internal variables from solutions for one element.
        It it then called in assembly to build the total solutions database.
        Whatever the output, it will be gatered in an element-wise list
        it better be a dictionary {"attribute" : values, etc.}
        """
        raise NotImplementedError("Abstract method of base class FiniteElement")


class Elastic(Material):
    """ Elastic materials (abstract)."""
    pass

class LinearElastic(Elastic):
    """ Linear elastic materials (abstract)."""
    def postProcessElem(self, elType, x, Ue, argOpt):
        """ Computes strains and stresses
        Input
        ----------
        elType:
        x:
        Ue: 

        Output
        ----------
        returns a dictionnary
        sols["strains"]: list of np 2D arrays (1 per Gauss point)
             each is a column vector containing Bechterew basis values
        sols["stresses"]: same.
        """
        sols = {}
        sols["strains"] = []
        sols["stresses"] = []
        D = self.tangentOperator(elType.dim)
        for (gp, xi) in enumerate(elType.gaussPoints):
            B_x = elType.shapeFunGradMatrix(xi, x)
            # Append list
            sols["strains"] += [np.dot(B_x, Ue)]
            sols["stresses"] += [np.dot(D, sols["strains"][gp])]
        return sols


######################################################################

class LinearElasticIsotrope(LinearElastic):
    """ The easiest one. """

    def __init__(self,*args):#E,nu=.33
        """ Linear elastic isotrope constructors (2 options)
        1) the first argument is an instance of base class Elastic
           ---> it is copied and 
           ---> the possible other arguments are neglected
        2) else we suppose arg1 if E and arg2 is nu (default .33)
        """
        if isinstance(args[0], Elastic):
            self.E = args[0].E
            self.nu = args[0].nu
        elif len(args)>0:
            self.E = args[0]
            if len(args)==1:
                self.nu = .33
            else:
                self.nu = args[1]
        else:
            raise WrongArgumentError("Expecting one LinearElasticIsotrop instance or 1 to two floats.")
        self.K = (self.E)/3./(1.-2.*self.nu)     # Kelvin compressibility
        self.mu = (self.E)/2./(1.+self.nu)       # Kelvin shear

    def tangentOperator(self, dim):
        """
        BECHETEREW TENSOR BASIS (build with sqrt2:=r2)
            |   S11 | |  | C1111 C1122 C1133 r2.C1123 r2.C1113 r2.C11h2 | |   E11 |
            |   S22 | |  |       C2222 C2233 r2.C2223 r2.C2213 r2.C2212 | |   E22 |
            |   S33 | |  |             C3333 r2.C3323 r2.C3313 r2.C3312 | |   E33 |
            | r2S23 |=|  |                    2.C2323  2.C2313  2.C2312 |*| r2E23 |
            | r2S13 | |  | sym.                        2.C1313  2.C1312 | | r2E13 |
            | r2S12 | |  |                                      2.C1212 | | r2E12 |
            
            >((depends?)^2)
         """
        if dim == 1:
            return np.array([[self.E]])
        else:
            return 3.*self.K*proSph_bech4(dim) + 2.*self.mu*proDev_bech4(dim)
        # That is a plane strain case where E33 doesn't contribute to the trace
        # But the inverse would be plane stress ... ?

######################################################################

class ElastoPlastic(Elastic):
    """An elastic behavior plus a yield surface

    The user must provide a yield function as a callable.

    Attributes
    ----------
    elasType: a Materials/Elastic class
    yieldFun: callable yield function f(sig, p, ...)

    """

    def __init__(self, *args):
        """ Elastoplastic constructors (several options)
        1) If the first argument is an instance of the same class
           ---> it is copied and 
           ---> the possible other arguments are neglected
        2) Else we suppose 
           - arg[0] if an Elastic instance 
           - arg[1] is a callable giving the yield function 
        """

        # COPY CONSTRUCTOR
        if isinstance(args[0], ElastoPlastic): 
            args[0].elasType.__init__(self, args[0])# call appropriate constructor
            self.elasType = args[0].elasType        # store elastic class
            self.yieldFun = args[0].yieldFun

        # DEFAULT CONSTRUCTOR
        else:
            self.elasType = type(args[0])        # store elastic class
            self.elasType.__init__(self, args[0])# call appropriate constructor
            self.yieldFun = args[1]              # add yield function
    
    ### Various functions --------------------------------------------
    def tangentOperator(self, dim): 
        """ Either elastic or elastic-plastic 
        """
        return self.elasType.tangentOperator(self, dim)

    def postProcessElem(self, elType, x, Ue, epsP=None):
        """ Computes strains and stresses
        Input
        ----------
        elType:
        x:
        Ue: 
        epsP: list of plastic strain per gauss point

        Output
        ----------
        returns a dictionnary
        sols["strains"]: list of np 2D arrays (1 per Gauss point)
             each is a column vector containing Bechterew basis values
        sols["stresses"]: same.
        """
        sols = {}
        sols["strains"] = []
        sols["stresses"] = []
        D = self.tangentOperator(elType.dim)
        for (gp, xi) in enumerate(elType.gaussPoints):
            B_x = elType.shapeFunGradMatrix(xi, x)
            # Append list
            sols["strains"] += [np.dot(B_x, Ue)]
            if epsP is None: 
                sols["stresses"] += [np.dot(D, sols["strains"][gp])]
            else: 
                sols["stresses"] += [np.dot(D, sols["strains"][gp]-epsP[gp])]
        return sols

    def ThermoForcesFromViscoPotential(self):
        """ for Standard Generalized Material, we could return sigma from \partial{\Omega}/\partial{epsilondot} provided an additionnal function
        """
        raise NotImplementedError('Sorry.')

######################################################################

class PlasticInstabilities(ElastoPlastic):
    """ An elastoplastic behavior where plasticity localizes
    
    In addition to a classical ElastoPlastic instance, this class embeds:
    - lc, a material length (localization bands half thickness)
    - 

    """
    def __init__(self, elastoPlasMat, lc=1., plasticLod=.01, tangentModulus=0.):
        """ Assign elastoplastic attributes plus parameters.
        
        Input
        ----------
        elastoPlastMat: an ElastoPlastic instance
        lc : the internal length
        plasticLoad: float or callable used to increment the plastic load when instability starts
        tangentModulus: float or callable that returns H such that, during instability, \dot{\sigma} = H \dot p

        """
        ElastoPlastic.__init__(self, elastoPlasMat) # affect or corresponding quantities
        self.lc = lc/2.
        self.plasticLoad = plasticLod 
        self.tangentModulus = tangentModulus
        self.h = tangentModulus

    def setTangentModulus(self,H):
        self.h = H

    def updateTangentModulus(self, sig, deps):
        self.h = self.tangentModulus(sig, deps)


######################################################################
# ********** BECHTEREW BASIS PROJECTORS and Operations ***************
######################################################################

def identity_bech4(dim):
    """Fourth order Bechterew identity.
    A one-diagonal matrix of size:
        S= \sum_{k=1}^{dim} k
         = 1 + 2 + ... + dim
         = (dim*(dim+1))/2
    """
    if isinstance(dim, int) and 0 < dim < 4 :
        size = int((dim*(dim+1))/2)
        return np.diag(np.ones(size))
    else: 
        raise WrongDimensionError("Calling this operator should only be relevant in dimension 2 or 3. Works with 1 though.")

def proSph_bech4(dim):
    """ Return the 4th order spheric projector in Bechterew basis."""
    if dim == 2:
        a = np.array([[1, 1, 0]])
        return a.T.dot(a) * .5
    elif dim == 3:
        a = np.array([[1, 1, 1, 0, 0, 0]])
        return a.T.dot(a) / 3
    else: 
        raise WrongDimensionError("Calling this operator should only be relevant in dimension 2 or 3.")

def proDev_bech4(dim):
    """ Return the 4th order deviatoric projector in Bechterew basis."""
    return identity_bech4(dim) - proSph_bech4(dim)

def bech2canonical(tens):
    """ Canonical basis change for any Bechterew input
    
    Input
    ----------
    tens: (2D np.array) a tensor in bechterew basis, order 2 or 4

    Output
    ----------
    A np array with as many axes as tensorial orders, with the right values
    """

    # Find dimension
    if   len(tens)==6: dim=3
    elif len(tens)==3: dim=2
    elif len(tens)==1: return tens
    else: raise WrongInputError("Can't work with that array, wrong dim or type.") 

    # Find order
    if len(tens.shape)>1: # order = 4
        cano = np.zeros([dim, dim, dim, dim])
        raise NotImplementedError("Only with order 2 for now !!")
    else: # order = 2
        cano = np.zeros([dim, dim])
        # Assign diagonal terms
        for (i, value) in enumerate(tens[:dim-1]):
            cano[i,i] = value
        # Assign non diagonal terms
        if dim==3:
            cano[[1,2],[2,1]] = tens[3]/ROOT2
            cano[[0,2],[2,0]] = tens[4]/ROOT2
            cano[[0,1],[1,0]] = tens[5]/ROOT2
        else: #dim==2:
            cano[[0,1],[1,0]] = tens[2]/ROOT2

    return cano
    


######################################################################
# ********** Databases and dictionaries ******************************
######################################################################

# ********** Criteria

def misesNorm(sig):
    """ Returns Mises norm given a bechterew-matrix stress  
       Should work if given a 2D np-array with one sig per row 
    """ 
    if isinstance(sig, float): return abs(sig)
    order = len(sig.shape)
    if order == 1: test = sig
    else: test = sig[0]
    if isinstance(test, float): return abs(sig)
    elif len(test)==6: dim=3
    elif len(test)==3: dim=2
    elif len(test)==1: return abs(sig)
    else: raise WrongInputError("Can't work with that sigma, wrong dim or type.") 
    return np.linalg.norm(np.dot(sig, proDev_bech4(dim)), axis=order-1)

def trescaNorm(sig):
    """ Returns Tresca norm given a bechterew-matrix stress""" 
    if isinstance(sig, float): return sig
    sig = bech2canonical(sig)
    sig,directions = np.linalg.eig(sig)
    maxShear = 0.
    for i in range(len(sig)):
        for j in range(i,len(sig)):
            s = abs(sig[i]-sig[j])
            if s > maxShear: maxShear = s
    return maxShear

critDict = {}
critDict['mises'] = misesNorm
critDict['tresca'] = trescaNorm

# ********** Yield functions

def myDirection(sig):
    """ ... """ 
    if isinstance(sig, float): return sig
    sig = bech2canonical(sig)
    sig,directions = np.linalg.eig(sig[:])
    #print("direct: ", directions)
    maxShear = 0.
    couple = [0,0]
    for i in range(len(sig)):
        for j in range(i,len(sig)):
            s = abs(sig[i]-sig[j])/2.
            if s > maxShear: 
                maxShear = s
                if sig[i] > sig[j]: couple = [i,j]
                else              : couple = [j,i]
    # (i,j) is the plan of max shear. What we want is a pi/4 rotation of those directions
    # from the biggest to the smallest (positive shear)
    g1  =  directions[couple[0]]+directions[couple[1]]
    g2  = -directions[couple[0]]+directions[couple[1]]
    #print("g1:", g1, "\ng2:", g2, "dotprod", np.dot(g1, g2))
    # give the appropriate sign to g2, such that the shear is positive
    #g2 *= np.dot(np.dot(g1, sig), g2)
    base = np.array([ g1[0]*g2[0], g1[1]*g2[1], (g1[0]*g2[1]+g1[1]*g2[0])/ROOT2 ])
    #print("base:", base/np.linalg.norm(base))
    return base/np.linalg.norm(base)
