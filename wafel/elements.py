import numpy as np
from math import sqrt
from wafel import materials as mat

"""
Purpose: define a set of element types with associated integration operators.
    [x] iso-parametric elements only
    [x] no beams or things with fancy dofs ... shold look into it though
    [x] Gauss quadrature only
"""

ROOT2 = sqrt(2.)
ROOT3 = sqrt(3.)

class FiniteElement:

    # COMMON attributes
    degree = None
    dim = None
    dof=None
    integCst = None
    gaussPoints = None
    gaussWeights = None

    # ABSTRACT METHODS

    def shapeFun(self,xi):
        """
        Return shape function vector:

            { N1  N2  N3 ...}
            
            >(1 x nNodes)
        """
        raise NotImplementedError("Abstract method of base class FiniteElement")

    def shapeFunGrad(xi):
        """
        Return shape function gradients :

            | N1,xi  N1,eta  N1,zeta | _ | N11 N12 N13 |
            | N2,xi  N2,eta  N2,zeta | _ | N21 N22 N23 |
            | N3,xi  N3,eta  N3,zeta | _ | N31 N32 N33 |
            | ...    ...     ...     |   | ... ... ... |
            
            >(nDim x nNodes)

        NOTE: twe whole point is to ask a minimal amount of information at the creation of a new element
        """
        raise NotImplementedError("Abstract method of base class FiniteElement")

    # GENERAL METHODS

    def shapeFunMatrix(self,xi):
        """
        Return shape function matrix :

            | N1       N2       ..       |
            |    N1       N2       ..    |
            |       N1       N2       .. |
            
            >(nDof x nNodes*nDof)
        """
        s = self.shapeFun(xi) # the 1D array of shape functions
        I = np.identity(self.dof)*s[0] # first block N1
        for n  in range(len(s)-1):
            I=np.append(I, np.identity(self.dof)*self.shapeFun(xi)[n+1], axis=1)
        return I

    def shapeFunGradMatrix(self, xi, coords = None, scalar = False):
        """ Return shape function gradient matrix.
        BECHETEREW TENSOR BASIS (build with sqrt2) such that:

            |   E11 | | N11         N21         ..       |      | u1 |
            |   E22 | |     N12         N22        ..    |      | v1 |
            |   E33 | |         N13         N23       .. |      | w1 |
            | r2E23 |=|     N13 N12     N23 N22    .. .. |/r2 * | u2 |
            | r2E13 | | N13     N11 N23     N21 ..    .. |/r2   | v2 |
            | r2E12 | | N12 N11     N22 N21     .. ..    |/r2   | w2 |
                                                                | .. |
                                                                | .. |
                                                                | .. |
            
            >((depends?) x nNodes*nDim)
        
        Input
        ----------
        xi: the natural coordinates of evaluation (np array 1 dim)
        coords: if specified then Ni(x) will be used instead of Ni(xi).
                It is first computed using :
                <Ni,x Ni,y ..>=<Ni,xi Ni,eta> inv([J])
        """
        d = self.dim
        g = self.shapeFunGrad(xi)

        # If coords are specified then we want B(x)
        if coords is None:
            pass
        else:
            Jmat = self.jacobianMatrix(xi, coords)
            if len(Jmat)==1:
                for (n, dNxi) in enumerate(g):
                    g[n]=dNxi/Jmat
            else:
                j=np.linalg.inv(Jmat)
                for (n, dNxi) in enumerate(g):
                    g[n]=np.dot(dNxi, j)

        # When we want the simple gradient for a scalar, we're done here
        # That is the equivalent of shapeFunGrad above but for X, not xi
        # We directly return the transpose so that its right contraction 
        # with a nodal vector makes sense
        if scalar:
            if len(g.shape)==1: return g
            else              : return g.T

        # ELSE
        # The rest is about assigning numbers at the right position
        if d==1: 
            """ trivial <N11 N21 N31 ... >"""
            return g.T

        elif d==2: # 
            """ Block to append: foreach Ni
            |Ni1     |
            |    Ni2 |
            |Ni2 NI1 |/r2
            """
            # first block N1
            B = np.diag(g[0]) 
            # cross derivatives # g[i] = [Ni,1 Ni,2 Ni,3 ... ]
            B = np.append(B, np.fliplr([g[0]/ROOT2]), axis=0) 
            # Repeat for all shape functions
            for n  in range(len(g)-1):
                b = np.diag(g[n+1]) 
                b = np.append(b, np.fliplr([g[n+1]/ROOT2]), axis=0) 
                B = np.append(B, b, axis=1)
            return B

        elif d==3:
            raise NotImplementedError("haven't started 3D")

    def jacobianMatrix(self, xi, nodCoords):
        """
        Below, the first argument is the TRANSPOSE of nodCoords
            |X1 X2 X3 .. | | N1,xi  N1,eta  N1,zeta |
            |Y1 Y2 Y3 .. |*| N2,xi  N2,eta  N2,zeta |
            |Z1 Z2 Z3 .. | | N3,xi  N3,eta  N3,zeta |
                           | ...    ...     ...     |
            >(nDim x nNodes)

        """
        return np.dot(nodCoords.T, self.shapeFunGrad(xi))

    def jacobian(self, xi, nodCoords):
        Jmat=self.jacobianMatrix(xi, nodCoords)
        if len(Jmat)==1:
            return Jmat
        else: 
            return np.linalg.det(Jmat)

    def integrate(self, fun, nodCoords):
        """Self explanatory
            
          \int_{e_x } f(x) dx 
        = \int_{e_xi} f(x) detJ(x) dxi
        = \sum_{i  }  f_i  detJ_i  w_i * integ_cst

        Above,  ()_i is the evaluation at Gauss point i in whatever configuration
                w_i is the weight
                integ_cst is the thickness/area/etc.

        input
        ----------
        fun: either a function of xi or a xi-wise list of np.arrays
        nodCoords: 1D-np.array of nodal coordinates for the element

        output
        ----------
        element integral of fun(xi)
        """

        # If fun is a list
        if isinstance(fun, list) or isinstance(fun, np.ndarray):
            # Instantiate with the right dimension
            tmp = fun[0]*self.gaussWeights[0]*self.jacobian(self.gaussPoints[0], nodCoords)
            for (n, xi) in enumerate(self.gaussPoints[1:]):
                tmp += fun[n+1]*self.gaussWeights[n+1]*self.jacobian(xi, nodCoords)

        # If fun is a callable
        elif callable(fun):
            # Instantiate with the right dimension
            tmp = fun(self.gaussPoints[0]) *self.gaussWeights[0] *self.jacobian(self.gaussPoints[0], nodCoords)
            for (n, xi) in enumerate(self.gaussPoints[1:]):
                tmp += fun(xi)*self.gaussWeights[n+1]*self.jacobian(xi, nodCoords)

        else: 
            raise WrongArgumentTypeError("fun should either be a list or a callable")

        return tmp*self.integCst

class B2(FiniteElement): # 2 nodes Bar, 1D, linear shape functions
    """ Bar element, 2 nodes, linear shape functions."""
    def __init__(self, crossSection=1., dimension=1):
        self.dim = dimension
        self.degree = 1
        self.dof = 1
        self.gaussPoints = np.array([-1., 1.])/ ROOT3
        self.gaussWeights = np.array([1., 1.])
        self.integCst = crossSection

    def shapeFun(self,xi):
        """See definition in base class."""
        N1 = (1.-xi)/2.
        N2 = (1.+xi)/2.
        return np.array([N1, N2]) 

    def shapeFunGrad(self,xi):
        """See definition in base class."""
        return np.array([[-.5], [.5]]) 
    
    def coordProject():
        return None

    
        
class T3(FiniteElement):
    """ Triangular element, 3 nodes, linear shape functions."""
    def __init__(self, thickness=1., dimension=2):
        self.dim = dimension
        self.degree = 1
        self.dof = 2
        self.gaussPoints = np.array([
            [.5, .5],
            [0., .5],
            [.5, 0.], ])
        self.gaussWeights = np.ones(3)/6.
        self.integCst = thickness

    def shapeFun(self,xi):
        """See definition in base class."""
        N1 = 1.-xi[0]-xi[1]
        N2 = xi[0]
        N3 = xi[1]
        return np.array([N1, N2, N3]) 

    def shapeFunGrad(self,xi):
        """See definition in base class."""
        return np.array([[-1., -1.], 
                         [ 1.,  0.],
                         [ 0.,  1.]]) 

    def coordProject():
        """See definition in base class."""
        return None



class Q4(FiniteElement):
    """ Quadrilateral element, 4 nodes, linear shape functions."""
    def __init__(self, thickness=1., dimension=2):
        self.dim = dimension
        self.degree = 1
        self.dof = 2
        #self.gaussPoints = np.array([[0., 0. ]])
        #self.gaussWeights = np.ones(1)*2.
        self.gaussPoints = np.array([
            [-1./ROOT3, -1./ROOT3 ],
            [ 1./ROOT3, -1./ROOT3 ],
            [ 1./ROOT3,  1./ROOT3 ],
            [-1./ROOT3,  1./ROOT3 ] ])
        self.gaussWeights = np.ones(4)*1.
        self.integCst = thickness

    def shapeFun(self,xi):
        """See definition in base class."""
        N1 = 1./4.*(1.-xi[0])*(1.-xi[1])
        N2 = 1./4.*(1.+xi[0])*(1.-xi[1])
        N3 = 1./4.*(1.+xi[0])*(1.+xi[1])
        N4 = 1./4.*(1.-xi[0])*(1.+xi[1])
        return np.array([N1, N2, N3, N4]) 

    def shapeFunGrad(self,xi):
        """See definition in base class."""
        return np.array([[-1./4.*(1.-xi[1]) , -1./4.*(1.-xi[0])], 
                         [ 1./4.*(1.-xi[1]) , -1./4.*(1.+xi[0])],
                         [ 1./4.*(1.+xi[1]) ,  1./4.*(1.+xi[0])],
                         [-1./4.*(1.+xi[1]) ,  1./4.*(1.-xi[0])]]) 

    def coordProject():
        """See definition in base class."""
        return None
