
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import numpy as np
import pdb

''' ==================================================================
Data treatment functions
'''

def newField(ST,**kwargs):
    ''' One liner to generate a new field.
    Can be nodal or Gausswise, tensorial order 0 to 2, instantiated for one ore more increments
    '''
    # Default values
    opt = {'location': 'node', 'tensorialOrder' : 1, 'nSteps':-1}

    # User values if any
    for key, value in kwargs.items():
        #print(key, ' = ', value)
        opt[key] = value

    # Case: nodal request
    if   opt['location'] == 'node':  
        shape=[ST.nNodes]
        if opt['tensorialOrder'] == 0:
            pass
        elif opt['tensorialOrder'] == 1:
            shape[0] *= ST.dof
        else: 
            raise WhatDoYouWantFromMeError("I find this somehow unexpected at a node.")

    # Case: Gauss-wise request
    elif opt['location'] == 'gauss':
        shape=[ST.nGaussPoints]
        if opt['tensorialOrder'] == 0:
            shape += [1]
        elif opt['tensorialOrder'] == 1:
            shape += [ST.dim]
        elif opt['tensorialOrder'] == 2:
            shape += [int((ST.dim**ST.dim+ST.dim)/2)]
        else: 
            raise WhatDoYouWantFromMeError("I find this somehow unexpected at a node.")
    # Case: Something went wrong
    else: 
        raise WrongInputError("The key 'location' should either be 'node' or 'gauss'" )

    # Handle nSteps
    if opt['nSteps'] == -1:
        pass
    elif isinstance(opt['nSteps'],int) and opt['nSteps']>1 :
        shape += [opt['nSteps']]
    else :
        raise WrongInputError("nSteps value must be a integer >1.")

    # Conclude
    return np.zeros(shape)

def concatenateSolutions(BC, Uunk, Funk):
    ''' solvers.solveLinearSystem returns
    This function concatenates imposed and unknowns displacements and forces in a single vector.
    '''
    size = len(BC.Uimp)+len(BC.Fimp)

    U = np.zeros(size)
    U[BC.repU] = BC.Uimp
    U[BC.repF] = Uunk

    F = np.zeros(size)
    F[BC.repF] = BC.Fimp
    F[BC.repU] = Funk

    return U, F

''' ==================================================================
Plotting functions
'''

def vectorFields(stru, vecs):
    """ Plots quiver fields from vector values
    vecs is a dictionnary
    """
    nFig = len(vecs)
    fig = plt.figure()
    n = 1
    for name,values in vecs.items():
        ax = fig.add_subplot(1, nFig, n)
        ax.set_aspect("equal")
        im = ax.quiver(stru.nodes[:,0], stru.nodes[:,1], values[::2], values[1::2])
        ax.set_title(name)
        n+=1
    return

def scalarFields(stru, scal, Title=""):
    """ Plots scatter fields from scalar values
    scal is a dictionnary
    """

    nFig = len(scal)
    fig = plt.figure()
    n = 1

    for name,values in scal.items():

        if len(values)==stru.nGaussPoints: X = stru.gaussTable[:,1:]
        else  : X = stru.nodes[:,:]

        if X.shape[1] is 1: ########################## CASE: 1D
            if len(values.shape)>1: # case: more than one field in this item
                size = values.shape[1]
                ax = fig.add_subplot(nFig, 1 ,n)
                for s in range(size):
                    im = ax.plot(X, values,'.')
            else: # one field alone
                ax = fig.add_subplot(nFig, 1, n)
                im = ax.plot(X, values,'.')
            ax.set_title(name)
        else: ###################################### CASE: 2D
            if len(values.shape)>1: # case: more than one field in this item
                size = values.shape[1]
                for s in range(size):
                    ax = fig.add_subplot(nFig, size, (n-1)*size+s+1)
                    ax.set_aspect("equal")
                    im = ax.scatter(X[:,0], X[:,1], c=values[:,s])
                    ax.set_title(name)
                    plt.colorbar(im, ax=ax)
            else: # case: one field only
                ax = fig.add_subplot(nFig, 1, n)
                ax.set_aspect("equal")
                im = ax.scatter(X[:,0], X[:,1], c=values)
                ax.set_title(name)
                plt.colorbar(im, ax=ax)
        n+=1
        fig.suptitle(Title)
    return fig

def stressesAndStrains(stru, sig, eps):
    
    pass
