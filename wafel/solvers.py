import numpy as np
from wafel import assembly as AS
import pdb

def solveLinearSystem(ST, BC, SM=None):
    """Solves K.U=F
    The linear system always has the form
        /           \ /    \   /    \ 
        | Kuu | Kuf | |Uimp|   |Funk|
        |---- + ----| |----| = |----| 
        | Kfu | Kff | |Uunk|   |Fimp| 
        \           / \    /   \    / 
    where the partition is given by BC.repU and BC.ref

    Input
    ----------
    ST: a Structure instance 
        It holds the stifness matrix K
    BC: a BoundaryConditions instance compatible with the structure
        It holds Uimp, Fimp
    SM: "second membre", an added force known everywhere

    Output
    ---------- 
    Uunk: the unknown displacement vector
    Funk: the unknown force vector

    """
    Kuu = ST.K[ np.ix_(BC.repU, BC.repU) ]
    Kff = ST.K[ np.ix_(BC.repF, BC.repF) ]
    Kuf = ST.K[ np.ix_(BC.repU, BC.repF) ]
    Kfu = ST.K[ np.ix_(BC.repF, BC.repU) ]

    if SM is None:
        Uunk = np.linalg.solve( Kff, BC.Fimp - Kfu.dot(BC.Uimp) )
        Funk = Kuu.dot(BC.Uimp)+ Kuf.dot(Uunk)
    else: 
        Uunk = np.linalg.solve( Kff, BC.Fimp + SM[BC.repF] - Kfu.dot(BC.Uimp) )
        Funk = Kuu.dot(BC.Uimp)+ Kuf.dot(Uunk) - SM[BC.repU] 


    return Uunk, Funk

#def meanStrains(ST, U):
#    """ Computes a gauss weigthed average of the strain measure on one elements """ 
#    # Initialization
#    esize = ST.dim**2 - ST.dim
#    EPS = np.zeros(ST.nelems, , )
#    for (e, con) in enumerate(ST.elems): # for each element
#        # Idenify 
#        nod = ST.nodes[con]
#        # Build repartition vector in global system
#        rep = np.array([0]*len(con) * ST.elemTypes[e].dof )
#        r = np.arange(self.dof)
#        for (i, n) in enumerate(connect):
#            rep[i*self.dof+r] = n*self.dof + r #
#        for (g, xi) in enumerate(ST.elemTypes[e].gaussPoints) # for each gauss point
#            B_x = np.dot( shapeFunGradMatrix(xi, nod), U[rep] )
#            eps = 


