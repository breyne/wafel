import numpy as np


class Tensor:
    """
    Tensors are stored as cartesian numpy arrays (cart).
    They can also be represented in Becheterew basis (bech)
    or Voight (voig)
    """
    def __init__(self, order=1, dimension=3):
        self.ord = order
        self.dim = dimension
        self.cartesian = np.zeros(self.dim)
        if self.ord>1
        for n in range(self.ord-1):
            self.cart=np.tensordot(self.cart, self.cart, axes=0)
