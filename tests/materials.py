import sys
sys.path.append("../")

import pdb
import matplotlib.pyplot as plt
import numpy as np
from wafel import elements as EL
from wafel import materials as MX
from wafel import assembly as AS
import meshio as mio


if False:
    print("\n========== Bechterew projectors")
    print("Identities (all dims)----------")
    print(MX.identity_bech4(1))
    print(MX.identity_bech4(2))
    print(MX.identity_bech4(3))
    print("Spheric    (all dims)----------")
    print(MX.proSph_bech4(2))
    print(MX.proSph_bech4(3))
    print("Deviatoric (all dims)----------")
    print(MX.proDev_bech4(2))
    print(MX.proDev_bech4(3))

mat = MX.LinearElasticIsotrope(1., 0.2)

if False:
    print("\n========= Linear Elastic Isotrope (E=1, nu=.2)")
    print("Tangent operator D(1)=")
    print(mat.tangentOperator(1))
    print("Tangent operator D(2)=")
    print(mat.tangentOperator(2))
    print("Tangent operator D(3)=")
    print(mat.tangentOperator(3))

def myYieldFun(S,p):
    return MX.misesNorm(S) - 140. - 1000.*p

mat = MX.ElastoPlastic(mat, myYieldFun)
sig = np.array([[200, 100, 50 ]]).T

if False:
    print("\n========= Elastoplastic with mises linear")
    print("elas props: ",mat.E, mat.nu)
    print("f=",mat.yieldFun(sig, 0.),"(3 points in 1D)")
    print("f=",mat.yieldFun(sig.T, 0.),"(1 point in 3D)")

def plastoLod(a):
    return .02 * a
def tanMod(a):
    return -.02 / a
lc = 6.

mat = MX.PlasticInstabilities(mat, lc, plastoLod, tanMod)
pdb.set_trace()

if True:
    print("\n========= Elastoplastic with mises linear")
    print("elas props: ",mat.E, mat.nu)
    print("f=",mat.yieldFun(sig.T, 0.),"(1 point in 3D)")
    print("q=",mat.plasticLoad(1./.02))
    print("H=",mat.tangentModulus(.02))






