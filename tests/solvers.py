import sys
sys.path.append("..")

import pdb
import numpy as np
from wafel import elements as EL
from wafel import materials as MX
from wafel import assembly as AS
from wafel import solvers as SO
from wafel import helpers as HE

if True:
    print("\n ========== TEST BAR-2nod-1D")
    print("\nAssemble 10 elements of length 1.")
    
    nod=np.array([[0.,1.,2.,3.,4.,5.,6.,7.,8.,9.,10.]]).T
    con=np.array([[0,1],
                  [1,2],
                  [2,3],
                  [3,4],
                  [4,5],
                  [5,6],
                  [6,7],
                  [7,8],
                  [8,9],
                  [9,10]])
    Structure_elast = AS.Structure(
            nod, 
            con, 
            EL.B2(1.), 
            MX.LinearElasticIsotrope(1.) )
    
    print("\nAssign boundary conditions u(0)=0, f(l)=1")
    uimp = np.array([ [0,0,0.] ]) #, [1,0,0.]
    fimp = np.array([ [10,0,1.5] ])
    BC = AS.BoundaryConditions(uimp, fimp, Structure_elast)
    
    print("\nCall linear solver")
    U, F = SO.solveLinearSystem(Structure_elast, BC)
    print("U=", U)
    print("F=", F)
      
if True:
    print("\n ========== TEST T3")
    print("\n ========== Assemble 2 elements (square of side 1).")
    
    nod=np.array([[0.,0.],
                  [1.,0.],
                  [0.,1.],
                  [1.,1.]])
    con=np.array([[0,1,3],
                  [1,2,3],])
    Structure_elast = AS.Structure(nod, con, EL.T3(1.), MX.LinearElasticIsotrope(1.) )
    
    print("\nAssign boundary conditions u(bottom)=0, f(top right)=(1,0)")
    uimp = np.array([ [0,0,0.],
                      [0,1,0.], 
                      [1,0,0.],
                      [1,1,0.]])
    fimp = np.array([ [3,0,1.] ])
    BC = AS.BoundaryConditions(uimp, fimp, Structure_elast)
    print("Call linear solver")
    U, F = SO.solveLinearSystem(Structure_elast, BC)
    print("U=", U)
    print("F=", F)

    print("\nAssign boundary conditions u(bottom)=0, u(top)=(1,0)")
    uimp = np.array([ [0,0,0.],
                      [0,1,0.],
                      [1,0,0.],
                      [1,1,0.],
                      [2,0,0.],
                      [2,1,0.],
                      [3,1,0.],
                      [3,0,1.]])
    BC = AS.BoundaryConditions(uimp, fimp, Structure_elast)
    print("repf=", BC.repF)
    
    print("Call linear solver")
    Uinc, Finc = SO.solveLinearSystem(Structure_elast, BC)
    print("U=", Uinc)
    print("F=", Finc)

    U = HE.newField(Structure_elast)
    U = HE.newField(Structure_elast, location='node',  tensorialOrder=1, nSteps=-1)
    U[BC.repU] = BC.Uimp
    U[BC.repF] = Uinc

    sols  = Structure_elast.postProcess(U)

    print("Solutions (post-processing): ", sols)

      
