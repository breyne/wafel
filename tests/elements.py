
#import sys
#sys.path.append("../wafel")

import pdb
import numpy as np
from wafel import elements as elt

a=np.array([[1,2,3],[4,5,6]])
def myfunOne(xi):
    return 1.
def myfun(xi):
    return 1.*np.dot(a.T,a)

""" --------------------------------------------------------------"""
if True:
    print("\n ========== TEST BAR-2nod-1D")
    bar=elt.B2()
    
    print("\nshapeFun(.5)")
    print(bar.shapeFun(.5))
    
    print("\nshapeFunGrad(.5)")
    print(bar.shapeFunGrad(.5))
    
    print("\nshapeFunMatrix(.5)")
    print(bar.shapeFunMatrix(.5))
    
    print("\nshapeFunGradMatrix(.5)")
    print(bar.shapeFunGradMatrix(.5))
    
    nod = np.array([2, 5])
    print("\njacobianMatrix(xi=.5, nodes=(2,5))")
    print(bar.jacobianMatrix(.5, np.array([2, 5])))
    
    print("\njacobian(xi=.5, nodes=(2,5))")
    print(bar.jacobian(.5, nod))
    
    print("\nIntegrate f(xi)=1 on length 3 (node=2,5)")
    print(bar.integrate(myfunOne, nod))
    
    print("\nIntegral from instantiated object: ")
    Int = bar.integrate(myfun, nod)
    print(Int)
    
    print("\nIntegral direct call: ")
    Int = elt.B2().integrate(myfun, nod)
    print(Int)

""" --------------------------------------------------------------"""
if True:
    print("\n ========== TEST T3-2D")
    bar=elt.T3()
    
    xi = np.array([.3, .3])
    print("xi=", xi)
    
    print("\nshapeFun(xi)")
    print(bar.shapeFun(xi))
    
    print("\nshapeFunGrad(xi)")
    print(bar.shapeFunGrad(xi))
    
    print("\nshapeFunMatrix(xi)")
    print(bar.shapeFunMatrix(xi))
    
    print("\nshapeFunGradMatrix(xi)")
    print(bar.shapeFunGradMatrix(xi))


""" --------------------------------------------------------------"""
if True:
    print("\n ========== TEST Q4-2D")
    bar=elt.Q4()
    
    xi = np.array([.0, .0])
    print("xi=", xi)
    
    print("\nshapeFun(xi)")
    print(bar.shapeFun(xi))
    
    print("\nshapeFunGrad(xi)")
    print(bar.shapeFunGrad(xi))
    
    print("\nshapeFunMatrix(xi)")
    print(bar.shapeFunMatrix(xi))
    
    print("\nshapeFunGradMatrix(xi)")
    print(bar.shapeFunGradMatrix(xi))


