import sys
sys.path.append("..")

import pdb
import numpy as np
from wafel import elements as EL
from wafel import materials as MX
from wafel import assembly as AS

if True:
    print("\n ========== TEST BAR-2nod-1D")
    
    nod=np.array([[1,2]]).T
    print("\nelement stifness Ke for node (1,2) with E=1.")
    print( AS.stiffElem( EL.B2(), nod, MX.LinearElasticIsotrope(1.) ) )
    
    print("\n Assemble 10 elements of length 1.")
    
    nod=np.array([[0.,1.,2.,3.,4.,5.,6.,7.,8.,9.,10.]]).T
    con=np.array([[0,1],
                  [1,2],
                  [2,3],
                  [3,4],
                  [4,5],
                  [5,6],
                  [6,7],
                  [7,8],
                  [8,9],
                  [9,10]])
    Structure_elast = AS.Structure(nod, con, EL.B2(1.), MX.LinearElasticIsotrope(1.) )
    
    print("\nSystem stiffness matrix K=")
    print( Structure_elast.K )
    
    print("\nAssign boundary conditions u(0)=0, f(l)=1")
    uimp = np.array([ [0,0,0.1] ])
    fimp = np.array([ [10,0,1.5] ])
    BC = AS.BoundaryConditions(uimp, fimp, Structure_elast)
    print("Uimp=", BC.Uimp, "repu = ", BC.repU)
    print("Fimp=", BC.Fimp, "repf = ", BC.repF)

    print("\n Build an arbitrary Nodal force from equivForce")
    nGP = Structure_elast.nGaussPoints
    eps = np.ones([nGP,1])*.01
    FF = Structure_elast.equivForce(eps)
    print("Equiv force = ", FF)

if True:
    print("\n ========== TEST triangle T3")
    
    nod=np.array([[1,1], 
                  [1,2],
                  [2,1]])
    print("coordinates: ", nod)
    print("\nelement stifness Ke for coords with E=1.")
    print( AS.stiffElem( EL.T3(), nod, MX.LinearElasticIsotrope(1.) ) )
    
    print("\n Assemble 2 elements (square of side 1).")
    
    nod=np.array([[0.,0.],
                  [1.,0.],
                  [0.,1.],
                  [1.,1.]])
    con=np.array([[0,1,2],
                  [1,2,3],])
    Structure_elast = AS.Structure(nod, con, EL.T3(1.), MX.LinearElasticIsotrope(1.) )
    
    print("\nSystem stiffness matrix K=")
    print( Structure_elast.K )
    
    print("\nAssign boundary conditions u(bottom)=0, f(top right)=(1,0)")
    uimp = np.array([ [0,0,0.],
                      [0,1,0.], 
                      [1,0,0.],
                      [1,1,0.]])
    fimp = np.array([ [3,0,1.] ])
    BC = AS.BoundaryConditions(uimp, fimp, Structure_elast)
    print("Uimp=", BC.Uimp, "repu = ", BC.repU)
    print("Fimp=", BC.Fimp, "repf = ", BC.repF)

    print("\n Build an arbitrary Nodal force from equivForce")
    nGP = Structure_elast.nGaussPoints
    eps = np.ones([nGP,3])*.01
    FF = Structure_elast.equivForce(eps)
    print("Equiv force = ", FF)


if True:
    print("\n ========== TEST quadrangle Q4")
    
    nod=np.array([[0.,0.], 
                  [1.,-.001],
                  [1.,1.],
                  [0.,1.]])
    print("coordinates: ", nod)
    print("\nelement stifness Ke for coords with E=1.")
    K = AS.stiffElem( EL.Q4(), nod, MX.LinearElasticIsotrope(1.,.2) ) 
    print(K)

