import numpy as np
import pdb
import matplotlib.pyplot as plt
import meshio as mio

from wafel import materials as MX
from wafel import elements as EL
from wafel import SPAM
from wafel import helpers as HE


if False:
    print("\n ========== TEST BAR-2nod-1D")
    
    print("\n Assemble elements")
    
    nod = np.array([np.arange(0,100,1)]).T
    con = np.array([range(0,len(nod)-1),range(1,len(nod))]).T

    
    print("\n Create Spam.Structure")
    mat = MX.LinearElasticIsotrope(1.)
    def yieldSurf(sig): return 1.
    mat = MX.ElastoPlastic(mat, yieldSurf)
    mat = MX.PlasticInstabilities(mat, 10.)
    ST = SPAM.Structure(nod, con, EL.B2(1.), mat)
                       
    
    print("\n Test fields")

    f = lambda x:np.sin(x/25.* np.pi)
    
    gp_signal = f(ST.gaussTable[:,1:])
    sb_signal = f(ST.subsetTable[:,1:])
    gp_fromSb = ST.subsetToGaussPoints(sb_signal)
    
    HE.scalarFields(ST,{"Actural signal":gp_signal, "afterPartition":gp_fromSb})
    plt.plot(ST.subsetTable[:,1],sb_signal,'or')
    plt.show()

    print("\n Test integration")

    INT = 0.
    for i, gp_i in enumerate(ST.subsetIdx) :
        INT += ST.patch_integrate(gp_signal, i)

    print('sb_',i,") integral=", INT)
    print("Compare with classical integral: ", ST.integrate(gp_signal))


    print("\n Test matrices assembly")
    un = np.arange(len(ST.K))
    N = np.ones(len(gp_signal))
    
    ST.updateAugmentedStiffness(N)

    print("\n Test Boundary Conditions")
    uimp = np.array([[0,0,0]])
    fimp = np.array([[3,0,1]])
    BC = SPAM.BoundaryConditions(uimp, fimp, ST)
    
    print("\n Test solver")

    print("No right hand side, no active subset")
    utu, ftu = SPAM.solveSystem(ST,BC)
    print("No right hand side, all active subsets")
    BC.addToActiveSubsets(list(range(ST.subsetSize)))
    pdb.set_trace()
    utu, ftu = SPAM.solveSystem(ST,BC)
    
    pdb.set_trace()

if True:
    print("\n ========== TEST 2D T3")
    
    print("\n Assemble elements")
    nod = np.array([
        [0. , 0. ],
        [1. , 0. ],
        [1. , 1. ],
        [0. , 1. ],
        [0.5, 0. ],
        [1. , 0.5],
        [0.5, 1. ],
        [0. , 0.5],
        [0.5, 0.5]])
    con = np.array([
        [2, 6, 5],
        [2, 6, 5],
        [6, 8, 5],
        [6, 8, 5],
        [6, 3, 8],
        [6, 3, 8],
        [5, 8, 1],
        [5, 8, 1],
        [1, 8, 4],
        [1, 8, 4],
        [8, 7, 4],
        [8, 7, 4],
        [8, 3, 7],
        [8, 3, 7],
        [4, 7, 0],
        [4, 7, 0]])

    print("\n Create Spam.Structure")
    mat = MX.LinearElasticIsotrope(1.)
    def yieldSurf(sig): return 1.
    mat = MX.ElastoPlastic(mat, yieldSurf)
    mat = MX.PlasticInstabilities(mat, 1)
    ST = SPAM.Structure(nod, con, EL.T3(1.), mat)
                       
    
    print("\n Test fields")

    f = lambda x:np.sin(x[:,0]/2.* np.pi)*np.sin(x[:,1]/2.*np.pi)
    
    gp_signal = f(ST.gaussTable[:,1:])
    sb_signal = f(ST.subsetTable[:,1:])
    gp_fromSb = ST.subsetToGaussPoints(sb_signal)
    
    HE.scalarFields(ST,{"Actual signal":gp_signal, "afterPartition":gp_fromSb})
    plt.plot(ST.subsetTable[:,1],ST.subsetTable[:,2],'xr')
    plt.show()

    pdb.set_trace()

    #print("\n Test integration")

    #INT = 0.
    #for i, gp_i in enumerate(ST.subsetIdx) :
    #    INT += ST.patch_integrate(gp_signal, i)

    #print('sb_',i,") integral=", INT)
    #print("Compare with classical integral: ", ST.integrate(gp_signal))


    #print("\n Test matrices assembly")
    #un = np.arange(len(ST.K))
    #N = np.ones(len(gp_signal))
    #
    #ST.updateAugmentedStiffness(N)

    #print("\n Test Boundary Conditions")
    #uimp = np.array([[0,0,0]])
    #fimp = np.array([[3,0,1]])
    #BC = SPAM.BoundaryConditions(uimp, fimp, ST)
    #
    #print("\n Test solver")

    #print("No right hand side, no active subset")
    #utu, ftu, dp = SPAM.solveSystem(ST,BC)
    #print("No right hand side, all active subsets")
    #BC.addToActiveSubsets(list(range(ST.subsetSize)))
    #pdb.set_trace()
    #utu, ftu, dp = SPAM.solveSystem(ST,BC)
    #
    #pdb.set_trace()

