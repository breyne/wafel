import pdb
import sys

import meshio as mio
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri

from wafel import elements as EL
from wafel import materials as MX
from wafel import assembly as AS
from wafel import solvers as SO
from wafel import helpers as HE


""" PURPOSE
In one line, being able to get a Structure instance

"""


""" ==================================================================
*** ---------- STRUCTURE ---------------------------------------------
"""

# Read the mesh from gmsh-made file

msh = mio.read("tensileSampleT3.msh")
nodes = msh.points[:,[0,1]]
connectivity = msh.cells["triangle"]

# Define element type 

thickness = 1. # 1 mm 
element = EL.T3(thickness) # triangular element

# Define material, here linear-elastic-isotropic

E = 7E5 # Young modulus
nu=.4   # Poisson coefficient
material = MX.LinearElasticIsotrope(E,nu)

# Gather it up in a Structure instance

structure = AS.Structure(nodes, connectivity, element, material) # thickness .83 mm

""" ==================================================================
*** ---------- LOADINGS ----------------------------------------------
We define a unitary loading in the form of boundary conditions
"""
# Define reference boundary conditions by appending a list
# On each line of that list: [node index, direction index, imposed magnitude]

uimp = [[0,1,0.]]  # Start by locking vertical displacement at first node.

imposeDisplacement = 1. # that would be a millimeter

# To help create this list, the Structure method inposeAtNodes
# The input is: direction index, boolean-valued function and magnitude to impose

uimp += structure.imposeAtNodes(0, lambda x: x[:,0]> 3.7, imposeDisplacement ) # east: unitary X-displacement
uimp += structure.imposeAtNodes(0, lambda x: x[:,0]<-3.7, 0.)                  # west: zero X-displacement
uimp += structure.imposeAtNodes(1, lambda x: x[:,0]<-4.7, 0.)                  # west: zero Y-displacement
uimp = np.array(uimp)

# Turn that list into a BoundaryConditions instance

boundaryConditions = AS.BoundaryConditions(uimp, None, structure)

""" ==================================================================
*** ---------- SOLVING -----------------------------------------------
"""

Uunk, Funk = SO.solveLinearSystem(structure, boundaryConditions)

U, F = HE.concatenateSolutions(boundaryConditions, Uunk, Funk)

""" ==================================================================
*** ---------- POST-PROCESSING ---------------------------------------
"""

postpro = structure.postProcessing(U)

mises = MX.misesNorm(postpro['stresses'])

""" ==================================================================
*** ---------- PLOTTING ----------------------------------------------
"""

HE.scalarFields(structure,{'Von Mises norm (MPa) at Gauss points': mises})
plt.show()
HE.vectorFields(structure,{'Displacement': U})
plt.show()
