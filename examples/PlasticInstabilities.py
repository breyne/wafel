import sys
sys.path.append("..")

import pdb
import matplotlib.pyplot as plt
import numpy as np
from wafel import elements as EL
from wafel import materials as MX
from wafel import assembly as AS
from wafel import solvers as SO
import meshio as mio


mattt = MX.LinearElasticIsotrope(70000., 0.2)
matt = MX.ElastoPlastic(mattt, 'mises', ['linear', 1., 1., 1.])
mat = MX.PlasticInstabilities(matt, 5.)

print("\n========= Plastic instabilities (lc=5)")

print("\nUsing SquareT3.msh---------------------")
msh = mio.read("squareT3.msh")
nod = msh.points[:,[0,1]]
con = msh.cells["triangle"]
stru = AS.Structure(nod, con, EL.T3(1., 2), mat)
struPlas = AS.Structure(nod, con, EL.T3(1., 2), matt)
struElas = AS.Structure(nod, con, EL.T3(1., 2), MX.LinearElasticIsotrope(70000., 0.2))

print("\n========= Assign simple tractions BCS")
uimp = [[0,0,0.],[0,1,0.]]# block RBM
BCfree = AS.BoundaryConditions(np.array(uimp), None, stru)
uimp  = stru.imposeAtNodes(0, lambda x: x[:,0]==0. , 0.)
uimp += stru.imposeAtNodes(1, lambda x: x[:,1]==0. , 0.)
uimp += stru.imposeAtNodes(0, lambda x: x[:,0]==10. , 1.)
#uimp += stru.imposeAtNodes(1, lambda x: x[:,0]==10. , 0.)
BC = AS.BoundaryConditions(np.array(uimp), None, stru)
BCplc = AS.BoundaryConditions(np.array(uimp), None, stru)

print("\n========= Solve elastic system")
Uinc, Finc = SO.solveLinearSystem(stru, BC)

print("\n========= Post processing: stresses and strains")
U = np.zeros(stru.K.shape[0])
U[BC.repU] = BC.Uimp 
U[BC.repF] = Uinc

eps, sig = stru.strainsAndStresses(U)

gp = stru.gaussTable[:,1:]
if True: 
    #fig=plt.figure()
    #plt.scatter(gp[:,0], gp[:,1], c=np.linalg.norm(sig, axis=1))
    #plt.title("Mises elastest")
    #plt.colorbar()
    fig=plt.figure()
    ax1 = fig.add_subplot(231)
    ax2 = fig.add_subplot(232)
    ax3 = fig.add_subplot(233)
    ax4 = fig.add_subplot(234)
    ax5 = fig.add_subplot(235)
    ax6 = fig.add_subplot(236)
    im1 = ax1.scatter(gp[:,0], gp[:,1], c=sig[:,0])
    plt.colorbar(im1, ax=ax1)
    im2 = ax2.scatter(gp[:,0], gp[:,1], c=sig[:,1])
    plt.colorbar(im2, ax=ax2)
    im3 = ax3.scatter(gp[:,0], gp[:,1], c=sig[:,2])
    plt.colorbar(im3, ax=ax3)
    im4 = ax4.scatter(gp[:,0], gp[:,1], c=eps[:,0])
    plt.colorbar(im4, ax=ax4)
    im5 = ax5.scatter(gp[:,0], gp[:,1], c=eps[:,1])
    plt.colorbar(im5, ax=ax5)
    im6 = ax6.scatter(gp[:,0], gp[:,1], c=eps[:,2])
    plt.colorbar(im6, ax=ax6)
    plt.show()

z=np.array([[5.1, 5.1]])

if True:
    print("\n========= Build equivalent plastic force at point (5,5), intensity 5 percents")
    prof = mat.plasticProfile(stru, z) * 1.
    prof = abs(nod[:,0]-nod[:,1])
    prof = (3-prof)
    prof[prof<0.00001]=0.
    Psi, epsp = mat.equivForce(stru, sig, prof)
    BCfree.Fimp  = Psi[BCfree.repF]
    BCplc.Fimp  = Psi[BCplc.repF]
    BCplc.Uimp *= 0.

    print("\n========= Solve patch and post-process")
    dUinc, dFinc = SO.solveLinearSystem(stru, BC,Psi)
    dU = np.zeros(stru.K.shape[0])
    dU[BC.repU] = BC.Uimp 
    dU[BC.repF] = dUinc

    deps,dsig = struPlas.strainsAndStresses(dU, epsp)
    #if True: # Stress norm
    #    fig=plt.figure()
    #    plt.scatter(gp[:,0], gp[:,1], c=np.linalg.norm(dsig, axis=1))
    #    plt.colorbar()
    #    plt.show()
    
if False: 
    print("\n========= THE SOFT PATCH TRIAL")

    lset = 3.-np.linalg.norm(gp-z, axis=1)
    #lset = 2-abs(gp[:,0]-gp[:,1])
    #lset = 2-abs(gp[:,0]-5)
    #arg = sig
    #arg[lset<0.] = np.array([0.,0.,0.])
    lset[lset<0.] =0.
    arg = np.append(lset[:,None]/3, sig, axis=1)
    arg[lset<0.] = np.array([0., 0.,0.,0.])

    stru.updateStiffness(arg)
    dUinc, dFinc = SO.solveLinearSystem(stru, BC)
    dU = np.zeros(stru.K.shape[0])
    dU[BC.repU] = BC.Uimp 
    dU[BC.repF] = dUinc
    sol = stru.postProcessing( dU, arg)
    cau = sol["stresses"]
    gll = sol["totalStrains"]
    glp = sol["plasticStrains"]

    fig=plt.figure()
    for s in range(3):
        ax = fig.add_subplot(3, 3, s+1)
        im = ax.scatter(gp[:,0], gp[:,1], c=cau[:,s])
        ax.set_title("Cauchy")
        plt.colorbar(im, ax=ax)
    for s in range(3):
        ax = fig.add_subplot(3, 3, s+4)
        im = ax.scatter(gp[:,0], gp[:,1], c=gll[:,s])
        ax.set_title("TotStrain")
        plt.colorbar(im, ax=ax)
    for s in range(3):
        ax = fig.add_subplot(3, 3, s+7)
        im = ax.scatter(gp[:,0], gp[:,1], c=glp[:,s])
        ax.set_title("PlasStrain")
        plt.colorbar(im, ax=ax)
    plt.show()

    plt.scatter(gp[:,0], gp[:,1], c=np.linalg.norm(glp,axis=1))
    plt.colorbar()
    plt.title("plasto")
    plt.show()

    plt.scatter(gp[:,0], gp[:,1], c=np.linalg.norm(cau,axis=1))
    plt.colorbar()
    plt.title("Mises")
    plt.show()

    plt.quiver(nod[:,0], nod[:,1], (dU)[::2], (dU)[1::2])
    plt.title("Velocity")
    plt.show()


if True: # Stress and strains
    fig=plt.figure()
    ax1 = fig.add_subplot(231)
    ax2 = fig.add_subplot(232)
    ax3 = fig.add_subplot(233)
    ax4 = fig.add_subplot(234)
    ax5 = fig.add_subplot(235)
    ax6 = fig.add_subplot(236)

    im1 = ax1.scatter(gp[:,0], gp[:,1], c=dsig[:,0])
    plt.colorbar(im1, ax=ax1)
    im2 = ax2.scatter(gp[:,0], gp[:,1], c=dsig[:,1])
    plt.colorbar(im2, ax=ax2)
    im3 = ax3.scatter(gp[:,0], gp[:,1], c=dsig[:,2])
    plt.colorbar(im3, ax=ax3)

    im4 = ax4.scatter(gp[:,0], gp[:,1], c=deps[:,0])
    plt.colorbar(im4, ax=ax4)
    im5 = ax5.scatter(gp[:,0], gp[:,1], c=deps[:,1])
    plt.colorbar(im5, ax=ax5)
    im6 = ax6.scatter(gp[:,0], gp[:,1], c=deps[:,2])
    plt.colorbar(im6, ax=ax6)

    plt.show()

if True: # Bilan Psi-U
    fig=plt.figure()
    axl=fig.add_subplot(121)
    axr=fig.add_subplot(122)
    axl.set_title(r'$\Psi$')
    axr.set_title('$U$')
    axl.set_aspect('equal')
    axr.set_aspect('equal')
    axl.quiver(nod[:,0], nod[:,1], Psi[::2], Psi[1::2])
    axr.quiver(nod[:,0], nod[:,1], (dU)[::2], (dU)[1::2])
    plt.show()

pdb.set_trace()
