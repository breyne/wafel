import pdb
import sys

import meshio as mio
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri

from wafel import elements as EL
from wafel import materials as MX
from wafel import assembly as AS
from wafel import solvers as SO
from wafel import helpers as HE


""" PURPOSE
In one line, being able to get a Structure instance

"""


""" ==================================================================
*** ---------- STRUCTURE ---------------------------------------------
"""

# Build mesh
nodes = np.array([np.arange(0,100.,1.)]).T
connectivity = np.array([range(0,len(nodes)-1),range(1,len(nodes))]).T

# Define element type 

area = 1. # 1 mm²
element = EL.B2(area) # triangular element

# Define material, here linear-elastic-isotropic

E = 7E5 # Young modulus
nu=.4   # Poisson coefficient
material = MX.LinearElasticIsotrope(E,nu)

# Gather it up in a Structure instance

structure = AS.Structure(nodes, connectivity, element, material) # thickness .83 mm

""" ==================================================================
*** ---------- LOADINGS ----------------------------------------------
We define a unitary loading in the form of boundary conditions
"""
# Define reference boundary conditions by appending a list
# On each line of that list: [node index, direction index, imposed magnitude]

uimp = np.array([[0,0,0.],[len(nodes)-1,0,10.]])  # Lock first node and impose unitary displacement at last node

# Turn that list into a BoundaryConditions instance

boundaryConditions = AS.BoundaryConditions(uimp, None, structure)

""" ==================================================================
*** ---------- SOLVING -----------------------------------------------
"""

Uunk, Funk = SO.solveLinearSystem(structure, boundaryConditions)

U, F = HE.concatenateSolutions(boundaryConditions, Uunk, Funk)

""" ==================================================================
*** ---------- POST-PROCESSING ---------------------------------------
"""

postpro = structure.postProcessing(U)
eps = postpro['strains']
mises = postpro['stresses']

""" ==================================================================
*** ---------- PLOTTING ----------------------------------------------
"""

HE.scalarFields(structure,{ 'axial strain (-)': eps, 'Von Mises norm (MPa) at Gauss points': mises})
plt.show()
